#include "stdafx.h"
#include "CppUnitTest.h"
#include "Array.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace arreytest
{
	TEST_CLASS(UnitTest1)
	{
	public:

		TEST_METHOD(CTOR)
		{
			Array array;
		}

		TEST_METHOD(Isempty)
		{
			Array array;
			const bool expected = true;
			const bool actual = array.isEmpty();
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}
		TEST_METHOD(pushBack)
		{
			Array array;
			array.pushBack(16);
			const bool expected = false;
			const bool actual = array.isEmpty();
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}
		TEST_METHOD(get_by_index)
		{
			Array array;
			array.pushBack(16);
			const int expected = 16;
			const int actual = array[0];
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}
		TEST_METHOD(IsFull)
		{
			Array array;
			for (int i = 0; i < SIZE; ++i)
			{
				array.pushBack(5);
			}
			const bool expected = true;
			const bool actual = array.isFull();
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}
		TEST_METHOD(size)
		{
			Array array;
			for (int i = 0; i < 3;++i)
			{
				array.pushBack(3);
			}			
			const int expected = 3;
			const int actual = array.size();
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}
		TEST_METHOD(popback)
		{
			Array array; for (int i = 0; i < 3; ++i)
			{
				array.pushBack(3);
			}
			array.popback(2);
			const int expected = 2;
			const int actual = array.size();
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}
		TEST_METHOD(Insert)
		{
			Array array;
			array.pushBack(2);
			array.pushBack(4);
			array.pushBack(5);
			array.Insert(1, 3);
			const int expected = 3;
			const int actual = array[1];
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}
		TEST_METHOD(erase)
		{
			Array array;
			array.pushBack(2);
			array.pushBack(3);
			array.pushBack(4);
			array.erase(1);
			const int expected =4 ;
			const int actual = array[1];
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}
		
	};

}
