#pragma once
#ifndef Array_h__
#define Array_h__

#define  SIZE 10
class Array
{
private:
	int m_data[SIZE];
	const int  m_capacity;
	int m_size;

public:
	Array();
	bool isEmpty() const;
	bool isFull() const;
	const int& operator[](const int index) const;
	int& operator[](const int index);	
	void pushBack(const int element);
	void popback(const int index) ;
	int size() const;
	void Insert(int index, int value);
	void erase(int index);
	~Array();
	
	
	
	
};
#endif // Array_h__

