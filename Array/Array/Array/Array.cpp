#include "Array.h"
#include <iostream>
#include <cassert>



Array::Array()
	: m_capacity(SIZE)
	, m_size(0)
{
	
}

bool Array::isEmpty() const
{
	return m_size == 0;

}

const int& Array::operator[](const int index) const
{
	std::cout << "Array::operator[](const int index) const" << std::endl;

	return m_data[index];
}

int& Array::operator[](const int index)
{
	return  m_data[index];
}

Array::~Array()
{
	
}

void Array::erase(int index)
{
	for (int i = index; i < m_size;++i)
	{
		m_data[i] = m_data[i + 1];
	}	
	popback(m_size-1);
}

void Array::Insert(int index, int value)
{
	
	int* array = new int[m_size+1];
	for (int i = 0; i < index;++i)
	{
		array[i] = m_data[i];
	}
	array[index] = value;
	for (int i = index + 1; i <= m_size+1;++i)
	{
		array[i] = m_data[i-1];
	}
	for (int i = 0; i < m_size + 1;++i)
	{
		m_data[i] = array[i];
	}
	
}

void Array::popback(const int index)
{
	assert(m_size>=0);
	m_data[index] = 0;
	--m_size;
}

int  Array::size() const
{
	
	return this->m_size;
}

void Array::pushBack(const int element)
{
	m_data[m_size] = element;
	++m_size;
}

bool Array::isFull() const
{
	return m_size == m_capacity;
}
