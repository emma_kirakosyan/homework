#include <iostream>
#include "Array.h"

int main()
{
	Array age;
	
	age.pushBack(1);
	age.pushBack(2);
	age.pushBack(3);
	age.pushBack(4);
	age.pushBack(5);
	age.pushBack(6);
	for (int i = 0; i <6;++i)
	{
		std::cout << age[i] << " ";
	}
	std::cout << std::endl;
	age.erase(2);

	for (int i = 0; i < 6; ++i)
	{
		std::cout << age[i] << " ";
	}
	std::cout << std::endl;
	age.Insert(2, 11);
	for (int i = 0; i < 6; ++i)
	{
		std::cout << age[i] << " ";
	}
		
	return 0;
}