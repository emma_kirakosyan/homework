#include "stdafx.h"
#include "CppUnitTest.h"
#include "Less.h"
#include "Prefix.h"
#include "Greater.h"
#include <iostream>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{		
	TEST_CLASS(UnittestLess)
	{
	public:
		
		TEST_METHOD(LessCtor)
		{
			Less less(const int=2);		
			
		}
		TEST_METHOD(GreaterCtor)
		{
			Greater G(const int=2);
		}
		TEST_METHOD(TestLess)
		{
			Less a(2);
			const Less expected = true;
			const Less actual = a.operator()(3);
			Assert::AreEqual(expected, actual, L"", LINE_INFO());
		}

	};
}