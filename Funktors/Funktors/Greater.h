#ifndef Greater_h__
#define Greater_h__


class Greater
{
private:
	int m_greater;
	
public:
	Greater(const int greater);
	
	bool operator()(const int rhs) const;
	~Greater();
};

#endif // Greater_h__
