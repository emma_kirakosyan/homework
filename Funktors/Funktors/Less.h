#ifndef Less_h__
#define Less_h__


class Less
{
private:
	 int m_less;
public:
	
	Less(const int less);	
	bool operator()(const int rhs) const;
	~Less();
};

#endif // Less_h__
