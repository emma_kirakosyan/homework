#include   "Greater.h"


Greater::Greater(const int greater)
	:m_greater(greater)
{
}

bool Greater::operator()(const int rhs) const
{
	if (m_greater > rhs)
	{
		return true;
	}
	else
		return false;
}

Greater::~Greater()
{
}
