
#ifndef Prefix_h__
#define Prefix_h__
#include <string>

class Prefix
{
private:
	const std::string  m_first;
	
public:
	Prefix(const std::string first);
	Prefix();
   	const std::string operator()(const std::string rhs) const;
	~Prefix();
};
#endif // Prefix_h__

