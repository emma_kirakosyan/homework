#include "Prefix.h"
#include <string>
#include <iostream>



Prefix::Prefix()
{

}


Prefix::Prefix(const std::string first)
	:m_first(first)
{

}

const std::string Prefix::operator()(const std::string rhs) const
{
	return (m_first + rhs);

}

Prefix::~Prefix()
{
}
